package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {

    int max_size = 20;
    ArrayList<FridgeItem> fridge = new ArrayList<FridgeItem>();
    ArrayList<FridgeItem> expired = new ArrayList<FridgeItem>();
    ArrayList<FridgeItem> notexpired = new ArrayList<FridgeItem>();

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        int Count = 0;
        for(FridgeItem element : fridge) {
            Count ++;
        }
        return Count;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() >= totalSize()) {
            return false;
        } 
        else {
            fridge.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)) {
            fridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        for(FridgeItem element : fridge) {
            if (element.hasExpired() == true) {
                expired.add(element);
            }
            else {
                notexpired.add(element);
            }
        }
        fridge.clear();
        for(FridgeItem item : notexpired) {
            fridge.add(item);
        }
        return expired;
    }

}
